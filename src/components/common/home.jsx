import React, { Component } from 'react';
import { Grid, Box, Button } from '@material-ui/core'
import { createMuiTheme } from '@material-ui/core/styles';
import { MuiThemeProvider } from '@material-ui/core'

const theme = createMuiTheme({
  palette: {
    primary: {
      light: '#1780df',
      main: '#1780df',
      dark: '#1780df',
      contrastText: '#fff',
    },
    secondary: {
      light: '#fb815e',
      main: '#fb815e',
      dark: '#fb815e',
      contrastText: '#000',
    },
  },
});

class Home extends Component {
    state = {
        doctors: [
            { name: 'انوشیروان', degree: '', image: 'https://www.darmankade.com/UploadFiles/Doctor/131844551805265433darvishi.jpg', address: 'شهر ری' },
            { name: 'ممد نقی', degree: '', image: 'https://www.darmankade.com/UploadFiles/Doctor/131844551805265433darvishi.jpg', address: 'شهر ری' },
            { name: 'علی ', degree: '', image: 'https://www.darmankade.com/UploadFiles/Doctor/131844551805265433darvishi.jpg', address: 'شهر ری' },
            { name: 'تست تستی زاده', degree: '', image: 'https://www.darmankade.com/UploadFiles/Doctor/131844551805265433darvishi.jpg', address: 'شهر ری' },
        ]
    }
    render() {
        return (
            <MuiThemeProvider muiTheme={theme}>
                <Grid container spacing={2} >
                {this.state.doctors.map((doctor) =>
                    <Grid key={doctor.name} item xs={12} md={6} lg={3}>
                        <Box align="center" border={1} borderRadius={10} borderColor="primary.main">
                            <img src={doctor.image} alt={ doctor.name } title={ doctor.name }/>
                            <h1> {doctor.name} </h1>
                            <p > متخصص مغز و اعصاب </p>
                            <Button variant="contained" mb="10" color="primary">
                                نوبت بگیر
                            </Button>
                        </Box>
                    </Grid>
                )}
            </Grid>
            </MuiThemeProvider>
        );
    }
}

export default Home;
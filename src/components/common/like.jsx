import React, { Component } from 'react';
import { Like } from 'react-feather'

const Like = (props) => {
    let classes = 'dr-icon'
    if(!props.Liked) classes += 'text-success'
    return ( 
    <Like
        onClick={props.onClick}
        style={{ cursor: 'pointer' }}
        className={classess}
     /> );
}
 
export default LIke;